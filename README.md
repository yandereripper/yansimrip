# Yandere Simulator (Source)
*A full source code and model archive of Yandere Simulator's latest build.*

### Introduction
YandereDev, aka. Alex Mahan has been developing Yandere Simulator for 4 years, and still, no announcement has been made toward a Kickstarter build of the game. Through his previous despicable character and sloppy programming, this project has driven itself into the ground. The lackluster and extremely lazy methods that Alex has used to program his game has left it in an almost unplayable state, as FPS drops are common even on the highest end machines.

This is excluding his horrible demeanour and character that has been shown in the past, and extremely recently.
If you wish to know more about this, you can see the great two hour video called "The Rise and Fall of YandereDev", found [here.](https://www.youtube.com/watch?v=b6FhLpeq378)

This project aims to make the entire reconstructed source of the game public to all, under a GPL license.
Fuck you, Alex.

### Status
The current build is not playable under normal circumstances, however can be edited and cleaned up using Unity. Any help is appreciated through commits to improve the code to this repository. A separate branch will appear for new YandereDev builds (whenever they're infrequently released) to be later merged with the master branch.
**State: Editable**
